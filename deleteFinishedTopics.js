const _ = require('lodash')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Invalid arguments')

    if(!_.isObject(args.instance))
        throw new Error('Invalid arguments')

    args.instance.handleFinishedTopics(args)
    args.instance.topics.finished = _.omit(args.instance.topics.finished, _.keys(args.elements))
    args.instance.performPostOperations(args)  
}
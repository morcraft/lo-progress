const _ = require('lodash')
const getFinishedTopics = require('./getFinishedTopics.js')
const handleTopics = require('./handleTopics.js')
const handleFinishedTopics = require('./handleFinishedTopics.js')
const topics = require('./topics.js')
const updateFinishedTopics = require('./updateFinishedTopics.js')
const setProgressBarAmount = require('./setProgressBarAmount.js')
const updateVisualClue = require('./updateVisualClue.js')
const addFinishedTopics = require('./addFinishedTopics.js')
const deleteFinishedTopics = require('./deleteFinishedTopics.js')
const performPostOperations = require('./performPostOperations.js')
const render = require('./render.js')
const EventsEmitter = require('events')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Invalid arguments')

    if(!_.isObject(args.APIInstance))
        throw new Error('Invalid APIInstance')

    var self = this

    self.APIInstance = args.APIInstance
    self.events = new EventsEmitter()

    if(!_.isObject(args.courseData))
        throw new Error('A courseData object property is expected')

    self.courseData = args.courseData

    if(!_.isObject(args.topics)){
        if(args.showFeedback !== false){
            console.warn('Some topics were expected. Initializing with no topics')
        }
        args.topics = {}
    }
    
    self.topics = new topics(args)
    self.elements = []

    self.handleTopics = function(_args){
        return handleTopics(_.merge({
            instance: self
        }, _args))
    }

    self.handleFinishedTopics = function(_args){
        return handleFinishedTopics(_.merge({
            instance: self
        }, _args))
    }

    self.performPostOperations = function(_args){
        return performPostOperations(_.merge({
            instance: self
        }, _args))
    }

    self.deleteFinishedTopics = function(_args){
        return deleteFinishedTopics(_.merge({
            instance: self
        }, _args))      
    }

    self.addFinishedTopics = function(_args){
        return addFinishedTopics(_.merge({
            instance: self
        }, _args))
    }

    self.updateFinishedTopics = function(_args){
        return updateFinishedTopics(_.merge({
            instance: self
        }, _args))
    }

    self.getFinishedTopics = function(_args){
        return getFinishedTopics(_.merge({
            instance: self
        }, _args))
    }
    
    self.renderProgressBar = function(_args){
        const element = render(_args)
        self.elements.push(element)
    }

    self.updateVisualClue = function(){
        return updateVisualClue({
            instance: self
        })
    }

    self.setProgressBarAmount = function(_args){
        return setProgressBarAmount(_.merge({
            instance: self
        }, _args))        
    }
}

module.exports.prototype.render = render
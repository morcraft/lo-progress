const _ = require('lodash')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Invalid arguments')

    if(!_.isObject(args.instance))
        throw new Error('Invalid instance')

    args.instance.handleFinishedTopics(args)
    _.merge(args.instance.topics.finished, args.elements)
    args.instance.performPostOperations(args)
}
const _ = require('lodash')
const htmlToElement = require('./htmlToElement.js')

module.exports = function (args) {
    if(!_.isObject(args))
        throw new Error('Invalid arguments')

    if(!(args.target instanceof Element))
        throw new Error('Invalid target')

    if(!_.isFunction(args.template))
        throw new Error('Invalid template')

    const element = htmlToElement(args.template(
        args.templateArguments
    ))

    return args.target.appendChild(element)
}
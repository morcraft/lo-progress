const _ = require('lodash')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Invalid arguments')

    if(!_.isObject(args.instance))
        throw new Error('Invalid instance')

    return args.instance.APIInstance.makeRequest({
        component: 'finishedTopics',
        method: 'getLOFinishedTopics',
        arguments: {
            shortName: args.instance.courseData.shortName,
            unit: args.instance.courseData.unit,
        }
    })
}
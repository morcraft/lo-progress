const _ = require('lodash')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Invalid arguments')

    if(!_.isObject(args.instance))
        throw new Error('Invalid instance')

    const finished = {}
    _.forEach(args.instance.topics.finished, function(v, k){
        if(_.isObject(args.instance.topics.all[k])) 
            finished[k] = v
    })
    
    const title = 'Temas finalizados: ' + _.size(finished) + ' / ' + _.size(args.instance.topics.all)

    _.forEach(args.instance.elements, function(element){
        element.setAttribute('title', title)
    })
}